#!/bin/sh
# Rebuild the boot image and primitives.go.

# This can be used to get access to new primitives written in Go. See
# compiler/primitives.scm.

set -ex

go build ./cmd/conscheme
cd compiler
../conscheme -boot conscheme.image.pre-built -c '(compile-bytecode "main.scm" "conscheme.image")'
../conscheme -boot conscheme.image -c '(print-operations (current-output-port))' > ../vm/primitives.go
cp -f conscheme.image conscheme.image.pre-built
cd ..
go build ./cmd/conscheme
